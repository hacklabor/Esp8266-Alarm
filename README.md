# Alarmsirene #


## Beschreibung und Teile ##


- Umbau einer Alarmsirene auf Mqtt mit WLAN Anbindung
- EIN der Alarmanlage mit "11" auf Topic "AlarmIN"
- max. 30s Alarm

## Bauteile ##

- Sirene [https://www.amazon.de/gp/product/B00C94G57W/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B00C94G57W/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
- Wemos Mini D1 [https://www.amazon.de/gp/product/B01ELFAF1S/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B01ELFAF1S/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)
- Relais [https://www.amazon.de/gp/product/B06XDC7MPC/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1](https://www.amazon.de/gp/product/B06XDC7MPC/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)


## Schaltplan ##


![](/Elektro/AlarmSirene.png)
![](/Bilder/bild.jpg)