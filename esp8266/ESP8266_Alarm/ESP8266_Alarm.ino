/*
  Based on Basic ESP8266 MQTT example

  nderung Mqqt "AlarmIN" = "Alarm"
  nderung Mqqt "ALarmOUT" = "Alarm"
  usw.
  THK 22.12.17

*/




#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "geheim.h"
extern "C" {
#include "user_interface.h" //to set the hostname
}

// Update these with values suitable for your network.

const char* ssid = SECRET_SSID;
const char* password = SECRET_PASS;
const char* mqtt_server = SECRET_MQQT_Server;
char* espHostname = "esp-alarm01";

// MQTT Last will and Testament
byte willQoS = 0;
const char* willTopic = "lwt/esp-alarm01";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-alarm01";
boolean willRetain = true;

const int output1 = 5;

WiFiClient espClient;
PubSubClient client(espClient);

long AlarmStarTime = 0;
long AlarmMaxTime = 15000; //milli Sec
char msg[50];
int value = 0;

void setup() {



  Serial.begin(115200);
  setup_wifi();
  pinMode(output1, OUTPUT);
  digitalWrite(output1, HIGH);
  delay(50);
  digitalWrite(output1, LOW);
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  digitalWrite(output1, HIGH);
  delay(50);
  digitalWrite(output1, LOW);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  wifi_station_set_hostname(espHostname);
  WiFi.mode(WIFI_STA);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    if ((char)payload[1] == '1') {
      AlarmEIN();
    }
    else {
      AlarmAus();
    }
  }
}




void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);      // ... and resubscribe
      client.subscribe("AlarmIN");
    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 500 seconds before retrying
      delay(500);
    }
  }
}

void AlarmEIN() {

  digitalWrite(output1, HIGH);
  client.publish("AlarmOUT", "11");
  AlarmStarTime = millis();
  //Serial.println("Alarm ist EIN");

}

void AlarmAus() {

  digitalWrite(output1, LOW);
  client.publish("AlarmOUT", "10");
  //Serial.println("Alarm ist AUS");
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

//  long now = millis();
//  if (now - AlarmStarTime > AlarmMaxTime) {
//    AlarmAus();
//  }
}
